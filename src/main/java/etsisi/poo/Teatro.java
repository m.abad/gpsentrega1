package etsisi.poo;

public class Teatro implements Actividad{

      private int id;
    private String nombre;
    private String descripcion;
    private double duracion;
    private double coste;
    private int aforo;
    private int edadMax = 25;
    private int edadMin = 65;
    private double descuentoJoven = 0.50;
    private double descuentoPension = 0.70;

    public void descuentoJoven(int edad){
        if(edad < edadMax){
            setCoste(getCoste() * (getDescuentoJoven()) );
        }
    }
    public void descuentoPension(int edad){
        if(edad > edadMin){
            setCoste(getCoste() * (getDescuentoPension()) );
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getDuracion() {
        return duracion;
    }

    public void setDuracion(double duracion) {
        this.duracion = duracion;
    }

    public double getCoste() {
        return coste;
    }

    public void setCoste(double coste) {
        this.coste = coste;
    }

    public int getAforo() {
        return aforo;
    }

    public void setAforo(int aforo) {
        this.aforo = aforo;
    }

    public int getEdadMax() {
        return edadMax;
    }

    public void setEdadMax(int edadMax) {
        this.edadMax = edadMax;
    }

    public int getEdadMin() {
        return edadMin;
    }

    public void setEdadMin(int edadMin) {
        this.edadMin = edadMin;
    }

    public double getDescuentoJoven() {
        return descuentoJoven;
    }

    public void setDescuentoJoven(int descuentoJoven) {
        this.descuentoJoven = (double) descuentoJoven / 100;
    }

    public double getDescuentoPension() {
        return descuentoPension;
    }

    public void setDescuentoPension(int descuentoPension) {
        this.descuentoPension = (double) descuentoPension / 100;
    }
}
