package etsisi.poo;

public class Interfaz {

    public void crearUsuario(){}
    public void login(){}
    public void logout(){}
    public void crearActividad(){}
    public void crearPlan(){}
    public void eliminarPlan(){}
    public void listarPlanesDisponiblesFecha(){}
    public void listarPlanesDisponiblesPuntuacion(){}
    public void unirsePlan(){}
    public void listarPlanesSubscrito(){}
    public void listarPlanesBaratos(){}
    public void listarPlanesFecha(){}
    public void listarUsuariosPuntuacion(){}

}
