package etsisi.poo;

public class Usuario {

    public int idusuario;
    private String nombreusuario;
    private int edad;
    private int edadmin = 14;
    private int edadmax = 100;
    private int numeromovil;
    private String contraseña;
    private double puntuacion;






        private boolean checkNombreUsuario(String nombreusuario){
return true;
    }
        private boolean checkNumeroMovil(int numeromovil){
            return true;
    }
        private boolean checkEdad(int edad){
            if(edadmin > edad || edad > edadmax){
                System.out.println("Error edad incorrecta");
                return false;
            }else {
                return true;
            }

    }
        private boolean checkContraseña(String contraseña){
            return true;
        }

        public Usuario(String nombreusuario, int edad, int numeromovil, String contraseña, double puntuacion){
            boolean todocorrecto = true;
            checkContraseña(contraseña);
            checkEdad(edad);
            checkNumeroMovil(numeromovil);
            checkNombreUsuario(nombreusuario);

            this.nombreusuario = nombreusuario;
            this.edad = edad;
            this.numeromovil = numeromovil;
            this.contraseña = contraseña;
            this.puntuacion = puntuacion;

    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getEdadmin() {
        return edadmin;
    }

    public void setEdadmin(int edadmin) {
        this.edadmin = edadmin;
    }

    public int getEdadmax() {
        return edadmax;
    }

    public void setEdadmax(int edadmax) {
        this.edadmax = edadmax;
    }

    public int getNumeromovil() {
        return numeromovil;
    }

    public void setNumeromovil(int numeromovil) {
        this.numeromovil = numeromovil;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }
}
